<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreatePin()
    {
        /*$this->json('POST', '/pins', ['subscription_id'=>1, 'msisdn'=>'098318686'])
            ->seeJsonStructure([
                'pin_identifier'
            ]);*/

        $response = $this->call('POST', '/pins', ['subscription_id' => 123, 'msisdn' => '098318686']);
        $this->assertEquals(200, $response->status());
    }

    public function testSubscribersPin()
    {
        $response = $this->call('POST', '/subscribers/pin', ['subscription_id' => 123, 'pin_identifier' => 'XYZ', 'pin' => 999]);
        $this->assertEquals(200, $response->status());
    }

    public function testSubscribersDirect()
    {
        $response = $this->call('POST', '/subscribers/direct', ['subscription_id' => 456, 'msisdn' => '098318686']);
        $this->assertEquals(200, $response->status());
    }
}
