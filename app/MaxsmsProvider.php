<?php

namespace App;

class MaxsmsProvider implements SmsProvider
{
    /**
     * Singleton pattern
     */
    private static $instance;
    private function __construct(){}
    public static function getInstance(){
        if (MaxsmsProvider::$instance==null)
            MaxsmsProvider::$instance = new MaxsmsProvider();
        return MaxsmsProvider::$instance;
    }


    public function createPin($subscription_id, $mobile)
    {
        /** Consume api, any error or unexpected response, an exception is triggered
         *  ex: throw new Exception("Unavailable server");
         */
        return 'XYZ';
    }

    public function subscribersPin($subscription_id, $identifier, $pin)
    {
        /** Consume api, any error or unexpected response, an exception is triggered
         *  ex: throw new Exception("Unavailable server");
         */
        return str_random(3);
    }
}
