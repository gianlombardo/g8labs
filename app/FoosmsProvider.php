<?php

namespace App;

class FoosmsProvider implements  SmsProvider
{
    /**
     * Singleton pattern
     */
    private static $instance;
    private function __construct(){}
    public static function getInstance(){
        if (FoosmsProvider::$instance==null)
            FoosmsProvider::$instance = new FoosmsProvider();
        return FoosmsProvider::$instance;
    }


    public function createPin($subscription_id, $mobile)
    {
        /** Consume api, any error or unexpected response, an exception is triggered
         *  ex: throw new Exception("Unavailable server");
         */
        return 'XYZ';
    }

    public function subscribersPin($subscription_id, $identifier, $pin)
    {
        /** Consume api, any error or unexpected response, an exception is triggered
         *  ex: throw new Exception("Unavailable server");
         */
        return str_random(3);
    }

    public function subscribersDirect($subscription_id, $mobile)
    {
        /** Consume api, any error or unexpected response, an exception is triggered
         *  ex: throw new Exception("Unavailable server");
         */
        return str_random(3);
    }
}
