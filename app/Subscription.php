<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Subscription extends Model
{
    protected $table = 'subscriptions';
    protected $fillable = ['mobile', 'subscription_id', 'pin_id', 'subscriber_id'];

    private $errors;
    private $rules = array(
        'mobile' => 'required',
        'subscription_id' => 'required|numeric',
    );

    public function validate(){
        $v = Validator::make($this->attributes, $this->rules);
        if ($v->fails())
        {
            $this->errors = $v->messages();
            return false;
        }
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function scopePin($query, $value){
        if ($value!=null)
            $query->where('pin_id', $value);
    }
}
