<?php

namespace App;

interface SmsProvider
{
    public function createPin($subscription_id, $mobile);
    public function subscribersPin($subscription_id, $identifier, $pin);

}