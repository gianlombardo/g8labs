<?php

namespace App\Http\Controllers;

use App\FoosmsProvider;
use App\MaxsmsProvider;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Subscription;
use Mockery\CountValidator\Exception;

class SubscriptionController extends Controller
{

    /**
     * @param int $subscription_id - The subscription identifier
     * @param string $msisdn - The user's mobile phone number
     * @return string $return - A JSON string that contains pin identifier
     */
    public function createPin (){
        $subscription_id = \Request::get('subscription_id');
        $msisdn = \Request::get('msisdn');
        if (empty($subscription_id) || empty($msisdn))
            return response()->json(array("message"=>"Missing required fields"),500);

        // --- Consume Provider ------*
        $arrParms['pin_id'] = null;
        try {
            $arrParms['pin_id'] = MaxsmsProvider::getInstance()->createPin($subscription_id, $msisdn);
        }catch (Exception $e){
            return response()->json(array("message"=>$e->getMessage()),500);
        }


        // --- Save Subscription -----*
        $arrParms['subscription_id'] = $subscription_id;
        $arrParms['mobile'] = $msisdn;
        $objSubscription = new Subscription($arrParms);
        if ($objSubscription->validate()) {
            $objSubscription->save();

            $return['pin_identifier'] =$objSubscription->pin_id;
            return response()->json($return);
        } else
            return response()->json(array("message"=>"Failed to validate fields"),500);
    }

    /**
     * @param int $subscription_id - The subscription identifier
     * @param string $pin_identifier - The identifier of the pin attempt
     * @param string $pin - The user's verification pin
     * @return string $return - A JSON string that contains the new subscriber identifier
     */
    public function subscribersPin (){
        $subscription_id = \Request::get('subscription_id');
        $pin_identifier = \Request::get('pin_identifier');
        $pin = \Request::get('pin');
        if (empty($subscription_id) || empty($pin_identifier) || empty($pin))
            return response()->json(array("message"=>"Missing required fields"),500);

        // --- Consume Provider ------*
        $subscriber_id = null;
        try {
            $subscriber_id = MaxsmsProvider::getInstance()->subscribersPin($subscription_id, $pin_identifier, $pin);
        }catch (Exception $e){
            return response()->json(array("message"=>$e->getMessage()),500);
        }

        // --- Update Subscription ---*
        try {
            $objSubscription = Subscription::where('subscription_id', $subscription_id)->where('pin_id', $pin_identifier)->first();
            if ($objSubscription == null)
                return response()->json(array('message' => 'Subscription not found'), 500);

            $objSubscription->subscriber_id = $subscriber_id;
            $objSubscription->save();
            return response()->json(array('id' => $subscriber_id));
        }catch (Exception $e){
            return response()->json(array("message"=>$e->getMessage()),500);
        }
    }

    /**
     * @param int $subscription_id - The subscription identifier
     * @param string $msisdn - The user's mobile phone number
     * @return string $return - A JSON string that contains the new subscriber identifier
     */
    public function subscribersDirect (){
        $subscription_id = \Request::get('subscription_id');
        $msisdn = \Request::get('msisdn');
        if (empty($subscription_id) || empty($msisdn))
            return response()->json(array("message"=>"Missing required fields"),500);

        // --- Consume Provider ------*
        $arrParms['subscriber_id'] = null;
        try {
            $arrParms['subscriber_id'] = FoosmsProvider::getInstance()->subscribersDirect($subscription_id, $msisdn);
        }catch (Exception $e){
            return response()->json(array("message"=>$e->getMessage()),500);
        }

        // --- Save Subscription -----*
        $arrParms['subscription_id'] = $subscription_id;
        $arrParms['mobile'] = $msisdn;
        $objSubscription = new Subscription($arrParms);
        if ($objSubscription->validate()) {
            $objSubscription->save();

            return response()->json(array('id' => $objSubscription->subscriber_id));
        } else
            return response()->json(array("message"=>"Failed to validate fields"),500);
    }

}
